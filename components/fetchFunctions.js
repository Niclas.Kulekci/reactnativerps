const GameApi =
    {
        token: "",
        nameInput:"Niclas",
        getToken: function () {
            return fetch("http://localhost:8080/auth/token")
                .then((response) => response.text())
                .then(text => {
                    this.token = text
                })
        },

        setName: function () {
            return fetch('http://localhost:8080/user/name', {
                method: 'POST',
                body: this.nameInput,
                headers: {
                    'token': this.token,
                    // 'Content-Type': 'text/plain'
                },
            }).then((response) => response.text())
        },
        startGame: function () {
            return fetch('http://localhost:8080/games/start', {
                headers: {
                    'token': this.token
                }
            }).then((response) => response.json())
        },

        gameList: function () {
            return fetch('http://localhost:8080/games', {headers: {'token': this.token}})
                .then((response) => response.json())
        },
        joinGame: function (gameId) {
            return fetch('http://localhost:8080/games/join/' + gameId, {headers: {'token': this.token}})
                .then((response) => response.json())
        },
        gameStats: function () {
            return fetch('http://localhost:8080/games/status', {headers: {'token': this.token}})
                .then((response) => response.json())
        },
        makeMove: function (sign) {
            return fetch('http://localhost:8080/games/move/' + sign, {headers: {'token': this.token}})
                .then((response) => response.json())
        },
        gameId: function (gameId) {
            return fetch('http://localhost:8080/games/' + gameId,
                {
                    headers: {
                        'token': this.token
                    }
                })
                .then((response) => response.json())
        },
    };


// if (sessionStorage.getItem('token') == null) {
//     GameApi.getToken();
// }

export default GameApi;
