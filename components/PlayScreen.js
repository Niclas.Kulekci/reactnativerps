import React, {useState, useEffect, useContext} from 'react';
import { StyleSheet, Text, View ,Button,Image } from 'react-native'
import {GameStatusContext} from "../App";

const rock = require('../assets/rock.png') //import images
const paper = require('../assets/paper.png')
const scissors = require('../assets/scissors.png')
const bgColors = ['#1abc9c', '#3498db', '#9b59b6'];

const IMAGES = {
    'ROCK': rock,
    'PAPER': paper,
    'SCISSORS': scissors
}
const MOVES = ['ROCK', 'PAPER', 'SCISSORS'];

function winCondition(move, opMove) {
    if (move === opMove) {
        return 'You chose ' + move + ', its a draw!';
    }

    if (move === 'ROCK' && opMove === 'SCISSORS') {
        return 'You chose ' + move + ', you WON!';
    }

    if (move === 'PAPER' && opMove === 'ROCK') {
        return 'You chose ' + move + ', you WON!';
    }
    if (move === 'SCISSORS' && opMove === 'PAPER') {
        return 'You chose ' + move + ', you WON!';
    }
    if (move === 'ROCK' && opMove === 'PAPER') {
        return 'You chose ' + move + ', you LOST!';
    }
    if (move === 'PAPER' && opMove === 'SCISSORS') {
        return 'You chose ' + move + ', you LOST!';
    }
    if (move === 'SCISSORS' && opMove === 'ROCK') {
        return 'You chose ' + move + ', you LOST!';
    }
}

const PlayScreen = () => {
    const {gameStatus, setGameStatus} = useContext(GameStatusContext);
    const [counter, setCounter] = useState(false); //countdown state

    useEffect(() => { // countdown function using the state and a timer
        if (counter === false) return;

        if (counter === 0) {
            const random = Math.floor((Math.random() * MOVES.length));
            setGameStatus({
                ...gameStatus,
                opponentMove: MOVES[random],
                status: winCondition(gameStatus.move, MOVES[random])
            });
            return;
        }
        const timer = setTimeout(() => {
            setCounter(counter - 1)
        }, 500)
        return () => clearTimeout(timer)
    }, [counter])

    const setMove = (move) => {
        setGameStatus({
            ...gameStatus,
            move: move
        });

        setCounter(3);
    };

    const mainView = (
        <View style={{flexDirection: "row"}}>

            <View style={styles.buttonStyle}>
                <Button onPress={() => setMove('ROCK')} title=" Rock "/>
            </View>
            <View style={styles.buttonStyle}>
                <Button onPress={() => setMove('PAPER')} title=" Paper "/>
            </View>
            <View style={styles.buttonStyle}>
                <Button onPress={() => setMove('SCISSORS')} title="Scissor"/>
            </View>
        </View>
    );

    const resultView = (<>
        <Text
              style={styles.textStyle}>{gameStatus.status}</Text>
        <Image source={IMAGES[gameStatus.opponentMove]} style={styles.sign}/>
        <View style={styles.buttonStyle}>
            <Text style={styles.textStyle2}>Computers choice above</Text>
        <Button title="Play Again!" onPress={() => { setCounter(false); }}>

        </Button>
        </View>
    </>);

    const counterView = (
        <Text style={styles.counter}> {counter} </Text>
    );

    return (
        <View style={StyleSheet.compose({backgroundColor: bgColors[counter - 1]}, styles.container)}>
            {(counter === false) ?
                mainView
                :
                (counter > 0) ?
                    counterView
                    :
                    resultView
            }
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    counter: {
        fontSize: 150,
        color: 'white',
    },
    sign: {
        width: 100,
        height: 100,
        marginBottom:100,
        marginTop:150
    },
    button: {
        position: "absolute",
        bottom: 40
    },
    buttonStyle: {
        marginHorizontal: 20,
        marginTop: 5,
    },
    textStyle:{
        fontSize: 32,
        fontWeight:'bold'
    },
    textStyle2:{
        fontSize:20,
        fontWeight:'bold',
        marginBottom: 10
    }
});
export default PlayScreen;