import React, { useState, useEffect, useContext } from 'react';
import {SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, Button, TouchableOpacity} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import GameApi from '../components/fetchFunctions'
import {GameStatusContext} from "../App";

export default function ListScreen() {
    const { gameStatus, setGameStatus } = useContext(GameStatusContext);
    const navigation = useNavigation();
    const [games, setGames] = useState([]);

    useEffect(() => {
        GameApi.gameList().then((list) => {
            setGames(list)
        });
    }, []);

    const Item = ({title}) => (
        <View style={styles.item}>
            <Text style={styles.title}>{title}</Text>
        </View>
    );

    const renderItem = ({item}) => {
        return (
            <TouchableOpacity onPress={() => {
                    GameApi.joinGame(item.id).then((status) => {
                        setGameStatus(status);
                        navigation.navigate('Game');
                    });
            }
            }>
                <Text style={styles.item}>{item.name}</Text>
            </TouchableOpacity>
        );
    };

    return (
        <View style={styles.container}>
            <FlatList
                data={games}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
            <Button
                title="Refresh List"
                onPress={() => {
                    GameApi.gameList().then((list) => setGames(list));
                }}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        textAlign:'center',
        fontSize: 32,
        color:'#FAFAFA',
        backgroundColor: '#3F51B5',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        color: '#FAFAFA',
        fontSize: 32,
    },
});