import React, {useContext, useEffect, useRef} from "react";
import {StyleSheet, View, Text, Button} from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import GameApi from "../components/fetchFunctions";
import {GameStatusContext} from "../App";

const Separator = () => (
    <View style={styles.separator} />
);
export default function GameScreen() {
    const { gameStatus, setGameStatus } = useContext(GameStatusContext);
    const interval = useRef(null);

    useEffect(() => {
        interval.current = setInterval(() => {
            GameApi.gameStats().then(gameStatus => {
               setGameStatus(gameStatus);
            });
        }, 2000);

        return () => {
            clearInterval(interval.current);
        }
    }, []);


    return (
        <View style={styles.container}>
            <Text id="Result" style={styles.loremBlahBlah}>{gameStatus.gameResult}</Text>
            <Button style={styles.container}
                    title="Rock"
                    onPress={() => {GameApi.makeMove('ROCK')}}
            />
            <View style={styles.space}/>
            <Button style={styles.container}
                    title="Paper"
                    onPress={() => {
                        GameApi.makeMove('PAPER')
                    }}
            />
            <View style={styles.space}/>
            <Button style={styles.container}
                    title="Scissors"
                    onPress={() => GameApi.makeMove('SCISSORS')}
            />
            <View style={styles.space2}/>

        </View>

    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: 16,
        width:100,
        marginTop:350,
        marginLeft:180,
    },
    title: {
        textAlign: 'center',
        marginVertical: 8,
    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    separator: {
        marginVertical: 15,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    space: {
        width: 20,
        height: 20,
    },
    space2: {
        width: 50,
        height: 50,
    },
    loremBlahBlah: {
            fontFamily: "roboto-700",
            color: "#121212",
            height: 56,
            width: 257,
            lineHeight: 17,
            textAlign: "center",
            fontSize: 24,
            marginBottom: 150,
            marginLeft:-80,
            alignSelf: "left"
        }
    // container: {
    //     flex: 1
    // },
    // materialHeader1: {
    //     height: 56,
    //     width: 375,
    //     marginTop: 32
    // },
    // rockButton: {
    //     height: 44,
    //     width: 100,
    //     borderRadius: 100
    // },
    // rockButton2: {
    //     height: 44,
    //     width: 100,
    //     marginLeft: 80
    // },
    // rockRow: {
    //     height: 44,
    //     flexDirection: "row",
    //     marginTop: 415,
    //     marginLeft: 51,
    //     marginRight: 44
    // },
    // paperButton: {
    //     height: 44,
    //     width: 100,
    //     borderRadius: 100
    // },
    // paperButton2: {
    //     height: 44,
    //     width: 100,
    //     marginLeft: 80
    // },
    // paperRow: {
    //     height: 44,
    //     flexDirection: "row",
    //     marginTop: 28,
    //     marginLeft: 51,
    //     marginRight: 44
    // },
    // scissorsButton: {
    //     height: 44,
    //     width: 100,
    //     borderRadius: 100
    // },
    // scissorsButton2: {
    //     height: 44,
    //     width: 100,
    //     marginLeft: 80
    // },
    // scissorsRow: {
    //     height: 44,
    //     flexDirection: "row",
    //     marginTop: 25,
    //     marginLeft: 51,
    //     marginRight: 44
    // },
    // loremBlahBlah: {
    //     fontFamily: "roboto-700",
    //     color: "#121212",
    //     height: 56,
    //     width: 257,
    //     lineHeight: 17,
    //     textAlign: "center",
    //     fontSize: 24,
    //     marginTop: -300,
    //     alignSelf: "left"
    // }
});