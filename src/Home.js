import React, {useState} from 'react'
import {View, Text, Button, StyleSheet, Pressable, Alert, TextInput} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Link} from "react-router-native";
import GameApi from "../components/fetchFunctions";
import {Modal} from "react-native-web";
import {TouchableHighlight} from "react-native-gesture-handler";


export default function HomeScreen() {
    const navigation = useNavigation();
    // const [modalVisible, setModalVisible] = useState(false);
    const [userName, setUserName] = useState("");

    function onSubmitEdit() {
        return fetch('http://localhost:8080/user/name', {
            method: 'POST',
            body: userName,
            headers: {
                'token': GameApi.token,
                // 'Content-Type': 'text/plain'
            },
        }).then((response) => response.text())
    }

    return (

        <View style={styles.container}>

            {/*<Modal visible={false}>*/}
            {/*    <View style={styles.modalView}>*/}
            {/*        <Text>yeah baby</Text>*/}
            {/*    </View>*/}
            {/*</Modal>*/}

            <TextInput
                style={styles.input}
                onChangeText={setUserName}
                value={userName}
                placeholder="    Enter your UserName here"
            />
            <Button title="Submit" style={{marginBottom: 50, marginTop: -50, backgroundColor: '#2196F3', color: '#FAFAFA',width:40}}
                    onPress={onSubmitEdit}>
            </Button>
            <View style={styles.space2}/>

            <Button style={styles.container}
                    title="Start Game"
                    onPress={() => {
                        navigation.navigate('Game');
                        GameApi.startGame();

                    }}
            />
            <View style={styles.space}/>
            <Button style={styles.container}
                    title="VS AI"
                    onPress={() => navigation.navigate('PlayScreen')}
            />
            <View style={styles.space}/>
            <Button style={styles.container}
                    title="Join Game"
                    onPress={() => navigation.navigate('JoinList')}
            />
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: 16,
        width: 100,
        marginTop: 350,
        marginLeft: 180
    },
    title: {
        textAlign: 'center',
        marginVertical: 8,
    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    space: {
        width: 20,
        height: 20,
    },
    space2: {
        width: 20,
        height: 50,
        marginBottom:180,
        marginTop:-90
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    input: {
        height: 40,
        width: 200,
        borderWidth: 1,
        marginLeft: -45,
        marginBottom: 20,
        marginTop:-70
    },
    container2: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: 16,
        width: 50,
        marginTop: 350,
        marginLeft: 180
    },
});
