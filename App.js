import React, { useEffect, useState } from "react";
import {StyleSheet, Text, View, Button} from "react-native";
import { NativeRouter, Route, Switch, Link } from "react-router-native";
import GamePage from "./src/GamePage";
import JoinList from "./src/JoinList";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DetailsScreen from "./src/GamePage";
import HomeScreen from "./src/Home";
import GameScreen from "./src/GamePage";
import JoinScreen from "./src/JoinList";
import GameApi from "./components/fetchFunctions";
import PlayScreen from "./components/PlayScreen";



const GameStatusContext = React.createContext({});

const Stack = createStackNavigator();

function App() {
    const [gameStatus, setGameStatus] = useState({});

    useEffect(() => {
        GameApi.getToken();
    }, [])

    return (
        <GameStatusContext.Provider value={{gameStatus, setGameStatus}}>
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Home">
                    <Stack.Screen name="Home" component={HomeScreen}/>
                    <Stack.Screen name="Game" component={GameScreen}/>
                    <Stack.Screen name="JoinList" component={JoinScreen}/>
                    <Stack.Screen name="PlayScreen" component={PlayScreen}/>
                </Stack.Navigator>
            </NavigationContainer>
        </GameStatusContext.Provider>
    );
}

export default App;
export { GameStatusContext };
